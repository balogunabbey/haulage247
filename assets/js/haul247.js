$(function(){
  'use strict'

  // This template is mobile first so active menu in navbar
  // has submenu displayed by default but not in desktop
  // so the code below will hide the active menu if it's in desktop
  if(window.matchMedia('(min-width: 992px)').matches) {
    $('.haul-navbar .active').removeClass('show');
    $('.haul-header-menu .active').removeClass('show');
  }

  // Shows header dropdown while hiding others
  $('.haul-header .dropdown > a').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });

  // Showing submenu in navbar while hiding previous open submenu
  $('.haul-navbar .with-sub').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });

  // this will hide dropdown menu from open in mobile
  $('.dropdown-menu .haul-header-arrow').on('click', function(e){
    e.preventDefault();
    $(this).closest('.dropdown').removeClass('show');
  });

  // this will show navbar in left for mobile only
  $('#haulNavShow, #haulNavbarShow').on('click', function(e){
    e.preventDefault();
    $('body').addClass('haul-navbar-show');
  });

  // this will hide currently open content of page
  // only works for mobile
  $('#haulContentLeftShow').on('click touch', function(e){
    e.preventDefault();
    $('body').addClass('haul-content-left-show');
  });

  // This will hide left content from showing up in mobile only
  $('#haulContentLeftHide').on('click touch', function(e){
    e.preventDefault();
    $('body').removeClass('haul-content-left-show');
  });

  // this will hide content body from showing up in mobile only
  $('#haulContentBodyHide').on('click touch', function(e){
    e.preventDefault();
    $('body').removeClass('haul-content-body-show');
  })

  // navbar backdrop for mobile only
  $('body').append('<div class="haul-navbar-backdrop"></div>');
  $('.haul-navbar-backdrop').on('click touchstart', function(){
    $('body').removeClass('haul-navbar-show');
  });

  // Close dropdown menu of header menu
  $(document).on('click touchstart', function(e){
    e.stopPropagation();

    // closing of dropdown menu in header when clicking outside of it
    var dropTarg = $(e.target).closest('.haul-header .dropdown').length;
    if(!dropTarg) {
      $('.haul-header .dropdown').removeClass('show');
    }

    // closing nav sub menu of header when clicking outside of it
    if(window.matchMedia('(min-width: 992px)').matches) {

      // Navbar
      var navTarg = $(e.target).closest('.haul-navbar .nav-item').length;
      if(!navTarg) {
        $('.haul-navbar .show').removeClass('show');
      }

      // Header Menu
      var menuTarg = $(e.target).closest('.haul-header-menu .nav-item').length;
      if(!menuTarg) {
        $('.haul-header-menu .show').removeClass('show');
      }

      if($(e.target).hasClass('haul-menu-sub-mega')) {
        $('.haul-header-menu .show').removeClass('show');
      }

    } else {

      //
      if(!$(e.target).closest('#azMenuShow').length) {
        var hm = $(e.target).closest('.az-header-menu').length;
        if(!hm) {
          $('body').removeClass('az-header-menu-show');
        }
      }
    }

  });

  $('#azMenuShow').on('click', function(e){
    e.preventDefault();
    $('body').toggleClass('az-header-menu-show');
  })

  $('.az-header-menu .with-sub').on('click', function(e){
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  })

  $('.az-header-menu-header .close').on('click', function(e){
    e.preventDefault();
    $('body').removeClass('az-header-menu-show');
  })

});
